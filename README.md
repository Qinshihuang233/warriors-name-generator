# Warriors Name Generator

A Clan cats’ name generator based on Erin Hunter’s *Warriors* book series.

This project is based on the project, （[https://github.com/nminnow/warriors-name-generator](https://github.com/nminnow/warriors-name-generator)）modified the css style and storage rules, speed up the site, such as inheritance maintenance work.

It will randomly generate a name in the format of `Prefixsuffix`, where all prefixes and suffixes comes from cats of the official *Warriors* universe.

Data is collected from [Crystal Pool](https://crystalpool.cxuesong.com/)ʼs [RDF dump](https://gitlab.com/Qinshihuang233/mirror-image). (In order to increase the speed of domestic users, the original GitHub project was forked to gitlab)Processed query results are stored under `data/`; note that they comply with CC BY-SA.

Published site: [wname.br2112.top](https://wname.br2112.top) [wname2.br2112.top](https://wname2.br2112.top)(Can be used)

Currently available in

* [English](https:wname2.br2112.top／en)
* [中文（大陆）](https://wname2.br2112.top)
* [繁體中文（臺灣）](https://wname2.br2112.top/zh-tw)
# # **猫武士名生成器**
基于艾琳-亨特的《猫武士》系列丛书的族猫名生成器。

本项目是在此项目（[https://github.com/nminnow/warriors-name-generator](https://github.com/nminnow/warriors-name-generator)）的基础上，修改了 css 样式和存储规则，加快了访问速度和项目的后续维护等工作。

它将随机生成前缀和后缀格式的名称，其中所有前缀和后缀均来自《猫武士》官方小说中的猫。

数据从[Crystal Pool](https://crystalpool.cxuesong.com/)的 [RDF](https://gitlab.com/Qinshihuang233/mirror-image) （为了让国内用户访问速度更快，fork了GitHub到了gitlab）中收集。处理后的查询结果存储在 data/ 下；请注意，它们符合 CC BY-SA。

发布网站：[wname.br2112.top](https://wname.br2112.top)

[wname2.br2112.top](https://wname2.br2112.top)（均可使用）

语言

[English](https://wname2.br2112.top/en)

[中文（大陆）](https://wname2.br2112.top)

[繁體中文（臺灣）](https://wname2.br2112.top/zh-tw)


